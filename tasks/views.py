from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def user_tasks(request):
    current_user = request.user
    assigned_tasks = Task.objects.filter(assignee=current_user)
    context = {
        "user_tasks": assigned_tasks,
    }
    return render(request, "tasks/list.html", context)
