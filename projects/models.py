from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, null=True, on_delete=models.CASCADE, related_name="projects"
    )

    def __str__(self):
        return str(self.name)


# Create your models here.
